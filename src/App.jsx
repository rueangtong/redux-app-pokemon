import { useState } from 'react'
import './App.css'
import { Provider } from 'react-redux'
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import store from './Store'
import Home from './components/Home/Home'
import Header from "./components/Header";
import PokemonDetail from "./components/PokemonDetail/PokemonDetail";

function App() {
  const [count, setCount] = useState(0)

    return (
        <div>
            <Provider store={store}>
                <Router>
                    <Header/>
                    <div className='container'>
                        <Routes>
                            <Route path='/Home' element={<Home/>} />
                                <Route path='Home/PokemonDetail/:name' element={<PokemonDetail/>} />
                        </Routes>
                    </div>
                </Router>
            </Provider>
        </div>
    )
}

export default App
