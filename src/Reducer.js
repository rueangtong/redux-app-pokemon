import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    pokemon:[]
}



const pokemonSlice = createSlice({
    name:"listPokemon",
    initialState,
    reducers: {
        addPokemon: (state, action) => {
            state.pokemon = action.payload
        }
    }
})
export const { addPokemon } = pokemonSlice.actions
export default pokemonSlice.reducer
