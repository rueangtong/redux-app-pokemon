import React from "react";
import  './ListPokemon.css'
import {useSelector} from "react-redux";
import ItemPokemon from "../ItemPokemon/ItemPokemon";


function ListPokemon() {

    const { pokemon } = useSelector((state) => state.pokemon )

    return(
        <div className= 'pokemon-container'>
            {pokemon && pokemon.map((pokemon) => (
                <ItemPokemon key={pokemon.name} pokemon={pokemon}/>))}
        </div>
    )

}
export default ListPokemon