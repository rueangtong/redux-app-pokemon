import React,{useState,useEffect}from "react";
import './PokemonDetail.css'
import pokemonAPI from "../../api/pokemonAPI";
import {useParams} from "react-router-dom";
import {Button,Grid} from "@mui/material";


function PokemonDetail() {
    const [pokemon,setPokemon] = useState([]);
    const {name} = useParams();
    let pokemons = []

    const addToFavorite = () =>{
        if (localStorage.getItem('pokemons')) {
            pokemons = JSON.parse(localStorage.getItem('pokemons'))
        }
        pokemons.push(pokemon.name)
        localStorage.setItem('pokemons',JSON.stringify(pokemons))
        fetchDetail();
    }

    const removeFavorite = () =>{
        if (localStorage.getItem('pokemons')) {
            pokemons = JSON.parse(localStorage.getItem('pokemons'))
        }
        let pokemons1  = pokemons?.filter((e) => e !== pokemon.name)
        pokemons = pokemons1
        localStorage.setItem('pokemons',JSON.stringify(pokemons))
        fetchDetail();
    }

    const fetchDetail = async () =>{
        const  res = await pokemonAPI.get(`${name}`)
        setPokemon(res.data)

    }
useEffect(() =>{
    if (localStorage.getItem('pokemons')) {
        pokemons = JSON.parse(localStorage.getItem('pokemons'))
    }
    fetchDetail();
}, []);

    const showFavorite = () => {
        let pokemonStock = []
        pokemonStock = JSON.parse(localStorage.getItem('pokemons'))
        return (
            pokemonStock?.some((e) => e === pokemon?.name)
        )
    }

    return (

        <div>
            <div className='pokemon-detail-con'>
                <div className='pokemon-detail-data'>
                    <h1>{pokemon.name}</h1>
                    {showFavorite() && (
                        <h1>
                            &hearts; Favorite
                    </h1>
                    )}
                    <h3>abilities :
                        { pokemon?.abilities?.map((item) => {
                            return <li>{item?.ability?.name}</li>;
                        }) }
                    </h3>
                    <h3>species : {pokemon?.species?.name}</h3>
                    <h3>weight : {pokemon?.weight}</h3>
                    <h3>Model : </h3>
                    <h1>
                    <img width="150" height="150" src = {pokemon?.sprites?.front_default} />
                    <img width="150" height="150" src = {pokemon?.sprites?.back_default} />
                    <img width="150" height="150" src = {pokemon?.sprites?.front_shiny} />
                    <img width="150" height="150" src = {pokemon?.sprites?.back_shiny} />
                    </h1>
                </div>
                <Button variant="contained" color="success" disabled={showFavorite()} onClick={addToFavorite}>
                    Add Favorite
                </Button>
                <Button variant="contained" color="error"  disabled={!showFavorite()} onClick={removeFavorite}>
                    Remove Favorite
                </Button>
            </div>
        </div>
    )

}
export default PokemonDetail