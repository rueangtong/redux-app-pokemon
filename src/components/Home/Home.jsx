import React, {useState,useEffect} from "react";
import pokemonAPI from "../../api/pokemonAPI";
import {useDispatch} from "react-redux";
import {addPokemon} from "../../Reducer";
import ListPokemon from "../ListPokemon/ListPokemon";

function Home() {
const  dispatch = useDispatch();


useEffect(() => {
    const fectPokemon = async () => {
        const res = await pokemonAPI.get()
        dispatch(addPokemon(res.data.results))
    }
    fectPokemon();
},[])

return(
    <div>
        <h3 style={{   margin: "1rem 0" }}> Pokemon Stock </h3>
        <ListPokemon />
    </div>
)
}
export default Home