import React from "react";
import './ItemPokemon.css'
import {Box} from "@mui/material";
import {Link, Route, Routes} from 'react-router-dom'
import Home from "../Home/Home";


import {Button} from "@mui/material";
import PokemonDetail from "../PokemonDetail/PokemonDetail";

const showFavorite = (name) => {
    let pokemonStock = []
    pokemonStock = JSON.parse(localStorage.getItem('pokemons'))
    return (
        pokemonStock?.some((e) => e ===  name)
    )
}

function ItemPokemon({pokemon}) {
    return (
        <div className='item'>
            <Box  mb={3}>
                <div className= 'item-content'>
                    <span className='item-name'>
                  {showFavorite(pokemon.name) && (
                      <>
                          &hearts;
                      </>
                  )}
                        { pokemon.name}
                    </span>
                </div>
                <div className='pokemon-action'>
                        <Button   as={Link} to={`PokemonDetail/${pokemon.name}`}>View Info</Button>
                </div>
            </Box>
        </div>
    )
}
export default ItemPokemon